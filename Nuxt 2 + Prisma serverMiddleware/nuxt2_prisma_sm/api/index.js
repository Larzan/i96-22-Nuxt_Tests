// index.js
import express from 'express'
import { expressjwt, ExpressJwtRequest } from 'express-jwt'
import { PrismaClient, Prisma } from '@prisma/client'

// done with: https://dev.to/prisma/adding-an-api-and-database-to-your-nuxt-app-with-prisma-2nlp

const prisma = new PrismaClient()

const debug = true

// if (process.env.NODE_ENV === 'production') {
//   prisma = new PrismaClient()
// } else {
//   if (!global.prisma) {
//     global.prisma = new PrismaClient()
//   }
//   prisma = global.prisma
// }

const app = express()
app.use(express.json())
// TODO: sanitize data


// --------------------------------------------
// AUTHENTICATION
// --------------------------------------------

  /**
   * LOGIN
   */
  app.post(`/auth/login`, async (req, res) => {
    if (debug) console.log('API/login: ', req.body);
    const user = await prisma.user.findUnique({
      where: {
        email: req.body.email
      },
      // include: { author: true }
    })
    if (debug) console.log('API/login: result  = ', user);
    // todo: verify password
    res.json(user)
  });


  /**
   * LOGOUT
   */
  app.post(`/auth/logout`, async (req, res) => {
    if (debug) console.log('API/logout: ', req.body);

    // const user = await prisma.user.findUnique({
    //   where: {
    //     email: req.body.email
    //   },
    //   // include: { author: true }
    // })
    // if (debug) console.log('API/login: result  = ', user);

    res.json({})
  });


  /**
   * USER
   */
  app.get(`/auth/user`, async (req, res) => {
    if (debug) console.log('API/user: ', req.body);
    // const user = await prisma.user.findUnique({
    //   where: {
    //     email: req.body.email
    //   },
    //   // include: { author: true }
    // })
    // if (debug) console.log('API/login: result  = ', user);
    res.json({})
  });



// --------------------------------------------



/**
 * Create a User
 * The first feature you’ll be implementing is creating a user/ author. The database will be expecting an email and
 * an optional name. It’s implementation is as follows:
 */
app.post(`/register`, async (req, res) => {
  if (debug) console.log('API/register: ', req.body);
  // const result = await prisma.user.create({
  //   data: {
  //     email: req.body.email,
  //     name: req.body.name,
  //   },
  // })
  // console.log('res: ', result);

  try {

    const result = await prisma.user.create ({
      data: {
        email: req.body.email,
        name: req.body.username,
        // TODO: encrypt the password
        password: req.body.password
      },
    })
    if (debug) console.log('API/register: result = ', result);
    res.json(result)

  } catch (e) {
    // https://www.prisma.io/docs/reference/api-reference/error-reference
    if (e instanceof Prisma.PrismaClientKnownRequestError) {
      let result = {
        error: [],
        email: req.body.email,
        name: req.body.username,
        password: '*********'
      }
      // The .code property can be accessed in a type-safe manner
      if (e.code === 'P2002') {
        console.log(
          'There is a unique constraint violation, a new user cannot be created with this email'
        )
        // console.log('ERROR: ', e.code, e.meta, e.message); meta = {target: ['email']}
        result.error.push('There is a unique constraint violation, a new user cannot be created with this email');
      } else {
        console.log('ERROR: ', e.code, e);
      }
      res.json(result)
    }
    // throw e
  }
})


/**
 * Creating a Post
 * Next, you’ll add the create post endpoint. The request body will expect a title, content and authorEmail.
 * If an author doesn’t exist in the database, their user record will be created.
 */
app.post('/post', async (req, res) => {
  const { title, content, authorEmail } = req.body
  const post = await prisma.post.create({
    data: {
      title,
      content,
      author: {
        connectOrCreate: {
          email: authorEmail
        }
      }
    }
  })
  res.status(200).json(post)
})


/**
 * Get drafts
 * Once that is done, you’ll need to be able to view all unpublished posts. Prisma lets you specify all relations you’d
 * like to be returned in the response with the include property. This is where you’ll add the author relation query to
 * view the respective posts as well as their authors.
 */
app.get('/drafts', async (req, res) => {
  const posts = await prisma.post.findMany({
    where: { published: false },
    include: { author: true }
  })
  res.json(posts)
})


app.get(
  "/protecteddrafts",
  expressjwt({ secret: "shhhhhhared-secret", algorithms: ["HS256"] }),
  async (req, res) => {

    if (!req.auth.admin) {
      return res.sendStatus(401);
    }

    const posts = await prisma.post.findMany({
      where: { published: false },
      include: { author: true }
    })
    res.json(posts)

    // res.sendStatus(200);
  }
);


/**
 * Get Post by Id
 * You can get a post by it’s id using findUnique as follows:
 */
app.get('/post/:id', async (req, res) => {
  const { id } = req.params
  const post = await prisma.post.findUnique({
    where: {
      id: Number(id),
    },
    include: { author: true }
  })
  res.json(post)
})


/**
 * Publish a Post
 * When a Post is ready to go live update the published field:
 */
app.put('/publish/:id', async (req, res) => {
  const { id } = req.params
  const post = await prisma.post.update({
    where: {
      id: Number(id),
    },
    data: { published: true },
  })
  res.json(post)
})


/**
 * Get Feed
 * All your published posts can be made available on the /feed endpoint, filtering them by checking that the published property is set to true.
 */
app.get('/feed', async (req, res) => {
  const posts = await prisma.post.findMany({
    where: { published: true },
    include: { author: true },
  })
  res.json(posts)
})


/**
 * Deleting a Post
 * The last CRUD feature is deleting a Post record in your database:
 */
app.delete(`/post/:id`, async (req, res) => {
  const { id } = req.params
  const post = await prisma.post.delete({
    where: {
      id: parseInt(id),
    },
  })
  res.json(post)
})


/**
 * Search for a Post
 */
app.get('/filterPosts', async (req, res) => {
  const { searchString } = req.query
  const draftPosts = await prisma.post.findMany({
    where: {
      OR: [
        {
          title: {
            contains: searchString,
          },
        },
        {
          content: {
            contains: searchString,
          },
        },
      ],
    },
  })
  res.send(draftPosts)
})




/**
 * logic for our api will go here
 */
export default {
  path: '/api',
  handler: app
}