
export const state = () => ({
  counter: 0
})


export const getters = {
  isAuthenticated(state) {
    return state.auth.loggedIn      // default from auth
  },

  loggedInUser(state) {
    return state.auth.user          // default from auth
  }
}


export const mutations = {
  increment(state) {
    state.counter++
  }
}