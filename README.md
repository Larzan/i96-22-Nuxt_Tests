# Boilerplate Nuxt

Use https://dev.auth.nuxtjs.org/ for normal user login and https://github.com/potato4d/nuxt-basic-auth-module for
.htaccess like basic authentication

Basic register / password reset / ... boilerplate here: https://www.digitalocean.com/community/tutorials/implementing-authentication-in-nuxtjs-app
